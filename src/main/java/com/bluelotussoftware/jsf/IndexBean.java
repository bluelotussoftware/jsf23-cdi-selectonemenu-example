/*
 * Copyright 2017 Blue Lotus Software, LLC.
 * Copyright 2017 John Yeary <jyeary@bluelotussoftware.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluelotussoftware.jsf;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author John Yeary <jyeary@bluelotussoftware.com>
 * @version 1.0.0
 */
@Named
@ViewScoped
public class IndexBean implements Serializable {

    private static final long serialVersionUID = -8409333287484097050L;

    private String selected;

    public IndexBean() {
    }

    public List<SelectItem> getItems() {
        List<SelectItem> items = new ArrayList<>();
        items.add(new SelectItem("", ""));
        items.add(new SelectItem("Tuna", "Tuna"));
        items.add(new SelectItem("Catfish", "Catfish"));
        items.add(new SelectItem("Gold Fish", "Gold Fish"));
        items.add(new SelectItem("Piranha", "Piranha"));
        return items;
    }

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }

}
